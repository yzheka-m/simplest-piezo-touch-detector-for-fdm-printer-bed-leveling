/*
   File:   main.h
   Author: nikolaypo

   Created on June 5, 2021, 1:06 PM
*/

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

//Length of sliding average filter, used for fast threshold detection
#define FilterSize 64 //samples per channel, 16..128 recommended

//Size of median filter, 2^n, n value, used for basel level detection
#define MedianOrder 16U

//Trigger threshold over median channel value to detect a touch
#define TriggerThreshold0 8 //Channel 0 threshold
#define TriggerThreshold1 8 //Channel 1 threshold
#define TriggerThreshold2 8 //Channel 2 threshold
#define TriggerThreshold3 8 //Channel 3 threshold

//Hold time
#define HoldTime 50 //Time for touch trigger signal prolongation, milliseconds

#include <stdint.h>
#include <avr/io.h>

typedef struct {
  unsigned Touch: 1; //Touch detected!
  unsigned ADCdata : 1; //ADC cycle completed, 4 channels converted
  unsigned LagDetected : 1; //Main code lag detected (debug purpose)
} Flags_t;

extern volatile Flags_t Flags; //Flags global declaration

//Define fatst trigger output actions, both for open drain and LED
//LED is on Arduino Nano board, D2 is open collector output
//Trigger output is active low (current sinc). Hi-Z when incactive.
#define TriggerOn() do{DDRD|=(1<<DDD2);PORTB|=1<<PORTB5;}while(0)
#define TriggerOff() do{DDRD&=~(1<<DDD2);PORTB&=~(1<<PORTB5);}while(0)

//Just LED control separately
#define LEDon() PORTB|=1<<PORTB5
#define LEDoff() PORTB&=~(1<<PORTB5)

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */
