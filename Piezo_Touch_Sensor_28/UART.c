#ifdef	__cplusplus
extern "C" {
#endif

#include <avr/io.h>

void UARTconfigure(void) {
    UBRR0 = 0x00; //1Mbaud speed (it is OK for Arduino Nano UART-USB converter)
    UCSR0A = 0x00; //RXC disabled; TXC disabled; UDRE disabled; FE disabled; DOR disabled; UPE disabled; U2X disabled; MPCM disabled;
    UCSR0B = 0x18; //RXCIE disabled; TXCIE disabled; UDRIE disabled; RXEN enabled; TXEN enabled; UCSZ2 disabled; RXB8 disabled; TXB8 disabled;
    UCSR0C = 0x0E; //UMSEL VAL_0x00; UPM VAL_0x00; USBS VAL_0x01; UCSZ 3; UCPOL disabled;
    PORTD |= 1 << PORTD0; //Turn on weak pull-up on Rx pin
    DDRD &= ~(1 << DDD0); //Make UART Rx pin an input
    DDRD |= 1 << DDD1; //Make UART Tx pin an output
    UCSR0B |= (1 << TXEN0); //USART0 Enable Tx;
    //UART Rx is not implemented yet thus not activated
};

void SendByte(uint8_t Byte){
    while (!(UCSR0A & (1 << UDRE0)));
    UDR0 = Byte;
}

#ifdef	__cplusplus
}
#endif
