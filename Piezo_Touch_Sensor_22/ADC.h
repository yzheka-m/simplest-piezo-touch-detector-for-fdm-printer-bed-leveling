#ifndef ADC_H
#define ADC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

#define BiasThreshold 233  //Should be less than 254

#if FilterSize > 255
  error("Filter size should not exceed 255. 16..64 recommended")
#endif

extern volatile uint8_t FilterOut[];  //Channel filter output values

void ADCconfigure(void);
void ADCstart(void);

#ifdef __cplusplus
}
#endif

#endif /* ADC_H */
