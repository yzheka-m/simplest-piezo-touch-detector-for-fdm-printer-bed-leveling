/*
   File:   main.h
   Author: nikolaypo

   Created on June 5, 2021, 1:06 PM
*/

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

  #define FilterSize 56 //Lenght of filter, samples per channel
  #define TriggerThreshold 16 //Threshold of filtered sensor value to decide the touch occured
  #define HoldTime 250 //Time for touch trigger signal prolongation, milliseconds
  
#include <stdint.h>
#include <avr/io.h>

typedef struct {
  unsigned Touch: 1; //Touch detected!
  unsigned ADCdata : 1; //ADC cycle completed, 4 channels converted
  unsigned LagDetected : 1; //Main code lag detected (debug purpose)
} Flags_t;

extern volatile Flags_t Flags; //Flags global declaration

//Define fatst trigger output actions, both for open drain and LED
//LED is on Arduino Nano board, D2 is open collector output
//Trigger output is active low (current sinc). Hi-Z when incactive.
#define TriggerOn() do{DDRD|=(1<<DDD2);PORTB|=1<<PORTB5;}while(0)
#define TriggerOff() do{DDRD&=~(1<<DDD2);PORTB &= ~(1<<PORTB5);}while(0)

//Just LED control separately
#define LEDon() PORTB|=1<<PORTB5
#define LEDoff() PORTB&=~(1<<PORTB5)

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */
